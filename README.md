# Back

## Install

```
$ cp config/database.sample.yml config/database.yml
$ cp .env.sample .env
$ docker-compose build
$ docker-compose run --rm web bundle exec rails db:setup
$ docker-compose up
```

## Tests & linters
```
$ docker-compose run --rm web bundle exec rspec
$ docker-compose run --rm web bundle exec rubocop
$ docker-compose run --rm web bundle exec reek
$ docker-compose run --rm web bundle exec brakeman -z -q
```
