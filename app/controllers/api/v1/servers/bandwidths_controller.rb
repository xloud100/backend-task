# frozen_string_literal: true

class Api::V1::Servers::BandwidthsController < Api::V1::ApiController
  def index
    bandwidths = Bandwidth.where(server_id: params['server_id'])

    data = ActiveModelSerializers::SerializableResource.new(bandwidths, each_serializer: BandwidthSerializer)

    render json: { data: data }
  end
end
