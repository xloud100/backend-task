# frozen_string_literal: true

class Api::V1::ServersController < Api::V1::ApiController
  def index
    servers = Server.all
    data = ActiveModelSerializers::SerializableResource.new(servers, each_serializer: ServerSerializer)

    render json: { data: data }
  end
end
