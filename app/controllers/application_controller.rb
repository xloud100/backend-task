# frozen_string_literal: true

class ApplicationController < ActionController::Base
  rescue_from ActiveRecord::RecordNotFound, with: :not_found_error

  def destroy
    head :ok
  end

  def not_found_error
    head :ok
  end
end
