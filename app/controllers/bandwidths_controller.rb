# frozen_string_literal: true

class BandwidthsController < ApplicationController
  def index
    @bandwidths = Bandwidth.where(server_id: params['server_id'])
  end
end
