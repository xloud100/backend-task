# frozen_string_literal: true

# == Schema Information
#
# Table name: bandwidths
#
#  id             :bigint           not null, primary key
#  interface_name :text             not null
#  value          :float            not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  server_id      :bigint
#  value_id       :integer          not null
#
# Indexes
#
#  index_bandwidths_on_server_id  (server_id)
#
# Foreign Keys
#
#  fk_rails_...  (server_id => servers.id)
#

class Bandwidth < ApplicationRecord
  belongs_to :server

  validates :value, :interface_name, presence: true
end
