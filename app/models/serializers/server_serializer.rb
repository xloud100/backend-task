# frozen_string_literal: true

# == Schema Information
#
# Table name: servers
#
#  id         :integer          not null, primary key
#  name       :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class ServerSerializer < ActiveModel::Serializer
  attributes :id, :name
end
