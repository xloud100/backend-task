# frozen_string_literal: true

require 'dry/monads/result'
require 'dry/monads/do'
require 'dry-validation'

class Operation
  include Dry::Monads::Result::Mixin
  include Dry::Monads::Do

  class Contract < Dry::Validation::Contract
    Dry::Validation.load_extensions(:monads)

    config.messages.default_locale = I18n.default_locale
    config.messages.backend = :i18n

    def self.call(*args)
      new.call(*args)
    end
  end

  def self.call(*args)
    new(*args).call
  end

  private

  def validate_params_with_contract(params, failure_key, contract: self.class::Schema)
    contract.call(params).to_monad.either(
      ->(result) { Success(result.to_h) },
      ->(failure) { Failure(failure_key => all_error_messages(failure.errors)) }
    )
  end

  def all_error_messages(errors)
    errors.to_h.transform_values do |error|
      case error
      when Hash
        all_error_messages(error)
      else
        error.join(', ')
      end
    end
  end
end
