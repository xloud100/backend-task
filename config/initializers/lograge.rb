# frozen_string_literal: true

Rails.application.configure do
  return unless Rails.env.staging? || Rails.env.production?

  # Prepend all log lines with the following tags.
  # config.log_tags = [:request_id]

  config.lograge.enabled = true
  config.lograge.formatter = Lograge::Formatters::Json.new
  config.lograge.base_controller_class = ['ActionController::API', 'ActionController::Base']
  config.lograge.custom_options = lambda { |event|
    {
      app: Rails.application.class.parent_name,
      params: event.payload[:params].except(:action, :controller).presence,
      user_agent: event.payload[:headers]['HTTP_USER_AGENT'],
      host: event.payload[:headers]['HTTP_HOST'],
      remote_addr: event.payload[:headers]['REMOTE_ADDR'],
      uuid: event.payload[:uuid],
      error_backtrace: event.payload[:exception_object]&.backtrace
    }.compact
  }
end
