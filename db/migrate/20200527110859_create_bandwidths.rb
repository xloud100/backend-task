class CreateBandwidths < ActiveRecord::Migration[6.0]
  def change
    create_table :bandwidths do |t|
      t.float :value, null: false
      t.text :interface_name, null: false
      t.serial :value_id, null: false
      t.references :server, foreign_key: true

      t.timestamps
    end
  end
end
