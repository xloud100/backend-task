# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

num_servers = (1..2)
num_bandwidth = (1..rand(2..10))
servers = []

num_servers.each { |n| servers << Server.create(name: "Server-#{n}") }
num_bandwidth.each do |n|
  Bandwidth.create(server: servers.sample, value: rand(10.0..100.0), interface_name: "bandwidth-#{n}")
end
