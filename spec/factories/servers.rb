# frozen_string_literal: true

# == Schema Information
#
# Table name: servers
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
FactoryBot.define do
  factory :server do
    sequence(:name) { |n| "Server-#{n}" }
  end
end
