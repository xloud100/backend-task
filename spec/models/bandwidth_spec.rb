# frozen_string_literal: true

# == Schema Information
#
# Table name: bandwidths
#
#  id             :bigint           not null, primary key
#  interface_name :text             not null
#  value          :float            not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  server_id      :bigint
#  value_id       :integer          not null
#
# Indexes
#
#  index_bandwidths_on_server_id  (server_id)
#
# Foreign Keys
#
#  fk_rails_...  (server_id => servers.id)
#

require 'rails_helper'

RSpec.describe Bandwidth, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of(:value) }
    it { is_expected.to validate_presence_of(:interface_name) }
  end
end
