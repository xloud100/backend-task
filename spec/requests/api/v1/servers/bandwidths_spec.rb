# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'API V1 Server' do
  let(:headers) { { 'Content-Type' => 'application/json' } }

  describe 'GET /api/v1/servers/:server_id/interfaces' do
    subject :get_interfaces do
      get "/api/v1/servers/#{server.id}/interfaces", headers: headers
    end

    let(:server) { create(:server) }
    let(:interfaces) { create_list(:bandwidth, 2, server: server) }

    before do
      get_interfaces
    end

    it 'returns list of interfaces at this server' do
      expect(response.status).to eq(200)
    end
  end

  describe 'DELETE to /api/v1/servers/:server_id/interfaces/:interface_id' do
    subject :delete_interface do
      delete "/api/v1/servers/#{serv.id}/interfaces/#{interface.id}", headers: { 'Content-Type' => 'application/json' }
    end

    let(:serv) { create(:server) }
    let(:interface) { create(:bandwidth, server: serv) }

    before do
      delete_interface
    end

    it 'removes this interface from this server' do
      expect(response.status).to eq(200)
    end
  end
end
