# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'API V1 servers' do
  let(:headers) { { 'Content-Type' => 'application/json' } }

  describe 'GET /api/v1/servers/' do
    subject :get_interfaces do
      get '/api/v1/servers/', headers: headers
    end

    let!(:server) { create(:server) }

    before do
      get_interfaces
    end

    it 'returns bandwidths interface list' do
      expect(response.status).to eq(200)

      data_server = JSON.parse(response.body).deep_symbolize_keys.dig(:data, :servers).first
      expect(data_server.fetch(:name)).to eq(server.public_send(:name))
    end
  end

  describe 'DELETE to /api/v1/servers/:server_id' do
    subject :delete_interface do
      delete "/api/v1/servers/#{server.id}", headers: headers
    end

    let(:server) { create(:server) }

    before do
      delete_interface
    end

    it 'removes bandwidths interface' do
      expect(response.status).to eq(200)
    end
  end
end
