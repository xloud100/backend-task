# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Bandwidths' do
  describe 'GET to /servers/:server_id/interfaces' do
    subject(:get_bandwidths) { get "/servers/#{server.id}/interfaces" }

    context 'when bandwidths exist' do
      let!(:server) { create(:server, id: 1) }
      let!(:bandwidth) { create(:bandwidth, server: server) }

      it do
        get_bandwidths

        expect(response.status).to eq(200)
        expect(response.body).to include(bandwidth.interface_name)
        expect(response.body).to include(bandwidth.value.to_s)
      end
    end
  end
end
